.PHONY: all
## Default target
all: build

# Docker image tags does not allow plus characters
# Unsupported for stable dev packages as pip name cannot contain minus char
APP_VERSION = $(shell env python3 setup.py --version | tr '+' '-')
IMAGE_NAME = docker.int.stcorp.no/snt/geomosaic:$(APP_VERSION)

.PHONY: build
build:
	docker build \
		--build-arg SETUPTOOLS_SCM_PRETEND_VERSION=$(APP_VERSION) \
		-t $(IMAGE_NAME) \
		.
# TODO: "release" rule, ensures dev version is not used!
# TODO: "clean" rule, removes all dev images
