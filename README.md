# GeoTIFF Mosaic

A Python tool for mosaicing geospatial datasets expressed as multi-band GeoTIFF
files. The tool takes as inputs a list of GeoTIFF datasets that represent
timeseries of multiple geospatial tiles, a region of interest described as Well
Known Text and creates a mosaic covering the envelope of the region of interest.

As each tile is added, any valid pixel replaces invalid or empty pixels of the
mosaic and 'better' pixels replace existing 'worse' pixels of the mosaic.
Better or worse is determined using the applicable classmap, as well as a
desirability score whose calculation is dataset type specific.

## Installation

First, retrieve the git repository, this requires that git is installed on the
system:

```sh
$ git clone git@<repo>
```

geomosaic has the following runtime and installation requirements:

* Python >= 3.6
* pip for Python 3
* GDAL binary utilities
* GDAL library
* GDAL Python bindings

The two most straightforward ways how to install these dependencies are either
through the package management system of the particular machine, or, by using
the ``conda`` package manager.

### Using system package manager

As an example, on Debian based systems:

```sh
$ sudo apt install python3.6 python3-pip gdal-bin libgdal-dev libgdal1i python3-gdal
```

### Using Conda package manager

With the [conda](https://conda.io/miniconda.html) package manager installed on
the system:

```sh
$ cd path/to/repository
$ conda env create -f environment.yml
$ source activate geomosaic
```

After the pre-requisites have been installed using either of the methods,
with an active Python>=3.6 environment with GDAL bindings installed:
```sh
$ pip install path/to/repository
```

### Docker

A `Dockerfile` describes the build and runtime dependencies.

## Usage

```sh
$ geomosaic <dataset_type> [OPTIONS] [ARGUMENTS]
```

For example:
```sh
$ geomosaic s2 -r wkt_file dataset1.tif dataset2.tif datasetN.tif
```

Alternatively:
```sh
$ geomosaic s2 -r wkt_file $(cat input_files)
```

Both of these commands will produce a mosaic in the current directory. For more
options see:
```sh
$ geomosaic <dataset_type> --help
```

## Implemented dataset types

### Sentinel-2 L2A

Sentinel-2 datasets converted from .SAFE to GeoTIFF beforehand. Each dataset is
assumed to consist of at least two files: ``s2id_10m.tif``, ``s2id_20m.tif``. The
60m file can be present, but is neglected. Only 10m and 20m mosaics are
created.

### Landsat 8 Collection 1 SR (OLI/TIRS)

Single file geoTIFF files containing all the desired bands. See
`landsat-singlefile` script.

### Landsat 4-7 Collection 1 SR (TM/ETM+)

Single file geoTIFF files containing all the desired bands. See
`landsat-singlefile` script.

## Extension

Additional dataset types can be implemented by subclassing the ``runner`` class,
and adding an additional CLI interface for the new dataset type in
``geomosaic/clitools/plugins``.

## Helper scripts

There are two helper scripts installed with the package:

``gtiff-quicklook`` creates a human-viewable quicklook of a given GeoTIFF file.
Default settings work well with Sentinel-2 datasets. Needs ``gdal_translate``
and ``convert`` (ImageMagick) CLI tools.

``safe2gtiff`` Converts Sentinel-2 datasets ('new' format) from .SAFE to
multi-band GeoTIFF. Needs ``gdal_merge.py`` CLI tool.

``landsat-singlefile`` Converts Landsat scene archives as they can be retrieved
from USGS to single geoTIFF files containing all the bands.


## Possible Geomosaic improvements

### Performance

Currently, according to the great Python profiler [py-spy](https://github.com/benfred/py-spy)
most of the time is spent in GDAL-land opening and closing datasets. However,
some speed improvements might be gained by using GPU processing. The places
where this could be relevant can be found by grepping for `@jit`, as this is
where the actual math happens that could be in principle pushed to the GPU.

### Memory footprint

Currently the whole mosaic-cube is being held in memory while values are being
added into it. This implies that the maximum memory usage is a sum of memory
it takes to hold the whole mosaic and memory it takes to hold a single
dataset. For large ROIs the memory footprint can become restrictive. For some
ROIs a VM with 16GB memory is not enough.

One way to reduce the memory use would be to cache the array to disk instead
of holding it in memory explicitly. One possible way to achieve this would be
to use `np.memmap`. Grep for `np.memmap` in the `geomosaic` processor code to
see where this should be done.

### Quality of the mosaic

This applies mostly to Sentinel2 mosaics.

The quality of the resulting mosaic depends in large part on the input data.
If we only have clear-sky input images, then we get a very good mosaic :).
Hence it is often a good idea to start with a low maximum cloud threshold to
get mostly clear-sky pictures of input data.

However, if we have to use more cloudier pictures, a particular problem is
cloud edges that are often visible in the final mosaic. This happens because the
original Sentinel2 cloud mask errs on the side of not marking something as cloud when
you can still see through it. So, cloud edges are often marked as 'clear'
pixels, and thus, never get exchanged by actual clear pixels from later
scenes.

There are a few possible options to explore to improve this:

#### Use per-pixel AOT score

Currently the only way the AOT (Aerosol Optical Thickness) layer of a dataset
is being used is by taking the mean of AOT over all pixels in a dataset and
then incorporating it into the 'goodness score' of the whole dataset.

It could be explored if the AOT is useful to find the hazy cloud edges by
plotting the layer and then comparing with the image. If this turns out to be
the case, the AOT layer could be used to construct an additional mask that
marks the cloud edge pixels as something else than 'clear-sky' in the mosaic,
such that these would be liable to be exchanged for actual 'clear-sky' pixels
later in processing.

The relevant part in code where this should be implemented is probably
`runner_s2.construct_replacement_mask`.

#### User a better cloud mask

A better cloud mask could be used that is more aggressive towards cloud edges.
It is probably the case that the CCN cloud mask that is part of the land-cover
thing could be separated out and used as a pre-processing step in the
geomosaic Dagger chain. It would also require having an additional
processor that takes the cloudmask and adds it to the S2 geoTiff file.

The cloudmask shouldn't come from a different file, as in this case the cloud
mask layer will not be included in the mosaic.

#### Maximize per-pixel NDVI values

Another distinct option to improve this would be to construct also an NDVI
layer for the mosaic and then aim to maximize per-pixel NDVI values in the
mosaic. This should also get rid of most of the cloud edge effects, as actual
clear-sky pixels will have a better NDVI than the hazy ones. This should also
hold true for areas with little vegetation and urban areas.

## Known issues

Newer GDAL versions (>3) seem to have switched the order of lat/lon values in
a dataset's geotransform. In particular, this causes incorrect envelope
calculation in `util.get_ds_envelope` and then further problems down the road.

Not sure if this is a bug in GDAL. GDAL versions <2.4 seem to work fine.

### Not all scenes are retrieved

Sometimes `s2-gtiff` is not able to fully retrieve data for some scenes, this
seems to affect certain tiles more than others, resulting in having holes in
the mosaic, in particular for Finnmark and Nordmark.

Usually this happens when many jobs are run at the same time on different
workers, it seems that something in the data retrieval pipeline can't handle
the load.

However, rerunning the jobs right after usually manage to retrieve the missing
data and all is well!
